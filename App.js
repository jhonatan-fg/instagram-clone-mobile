import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { View, Text } from 'react-native';
import firebase from 'firebase';
import 'firebase/firestore';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';



import LandingScreen from './components/auth/Landing';
import RegisterScreen from './components/auth/Register';
import LoginScreen from './components/auth/Login';
import AddScreen from './components/main/Add';
import SaveScreen from './components/main/Save';
import CommentScreen from './components/main/Comment'
import { LongPressGestureHandler } from 'react-native-gesture-handler';

import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import rootReducer from './redux/reducers';
import thunk from 'redux-thunk';

import MainScreen from './components/Main';

const store = createStore(rootReducer, applyMiddleware(thunk));

const firebaseConfig = {
	apiKey: 'AIzaSyC8-kowBb-G1i-laqMUHYA7Xv8pCKjzvcc',
	authDomain: 'instagram-dev-b8605.firebaseapp.com',
	databaseURL: "https://instagram-dev-b8605.firebaseio.com/",
	projectId: 'instagram-dev-b8605',
	storageBucket: 'instagram-dev-b8605.appspot.com',
	messagingSenderId: '56558162234',
	appId: '1:56558162234:web:689cecea6820eb8b350003',
	measurementId: 'G-SYJK7YD8KN',
};

if (firebase.apps.length === 0) {
	firebase.initializeApp(firebaseConfig);
}

const Stack = createStackNavigator();

export class App extends Component {
	constructor(props) {
		super();
		this.state = {
			loaded: false,
		};
	}

	componentDidMount() {
		firebase.auth().onAuthStateChanged((user) => {
			if (!user) {
				this.setState({
					loggedIn: false,
					loaded: true,
				});
			} else {
				this.setState({
					loggedIn: true,
					loaded: true,
				});
			}
		});
	}

	render() {
		const { loggedIn, loaded } = this.state;
		if (!loaded) {
			return (
				<View style={{ flex: 1, justifyContent: 'center' }}>
					<Text>Loading</Text>
				</View>
			);
		}
		if (!loggedIn) {
			return (
				<NavigationContainer>
					<Stack.Navigator initialRouteName='Landing'>
						<Stack.Screen
							name='Landing'
							component={LandingScreen}
							options={{ headerShown: false }}
						/>
						<Stack.Screen name='Register' component={RegisterScreen} />
						<Stack.Screen name='Login' component={LoginScreen} />
					</Stack.Navigator>
				</NavigationContainer>
			);
		}
		return (
			<Provider store={store}>
				<NavigationContainer>
					<Stack.Navigator initialRouteName='Main'>
						<Stack.Screen name='Main' component={MainScreen}/>
						<Stack.Screen name='Add' component={AddScreen} navigation={this.props.navigation}/>
						<Stack.Screen name='Save' component={SaveScreen} navigation={this.props.navigation}/>
						<Stack.Screen name='comment' component={CommentScreen} navigation={this.props.navigation}/>
					</Stack.Navigator>
				</NavigationContainer>
			</Provider>
		);
	}
}

export default App;
